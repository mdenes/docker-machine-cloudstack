variable "SSH_PUBKEY" {
  type = string
}

variable "DRIVER_MACHINE_NAME" {
  type = string
}
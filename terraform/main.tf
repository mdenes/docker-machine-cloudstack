resource "cloudstack_ssh_keypair" "default" {
  name = "Driver SSH Key"
  public_key = var.SSH_PUBKEY
}

resource "cloudstack_instance" "docker-machine-driver" {
  name = var.DRIVER_MACHINE_NAME
  service_offering = "Medium Instance"
  template = "debian 11.5 minimal"
  zone = "zone-ci"
  expunge = true
  keypair = "Driver SSH Key"
}

resource "cloudstack_security_group" "runners_group" {
  name        = "runners_group"
  description = "Security group for runners"
}

resource "cloudstack_security_group_rule" "runners_rules" {
  security_group_id = cloudstack_security_group.runners_group.id

  rule {
    cidr_list                = ["${cloudstack_instance.docker-machine-driver.ip_address}/32"]
    protocol                 = "tcp"
    ports                    = ["22", "2376"]
    traffic_type             = "ingress"
  }
}
module github.com/andrestc/docker-machine-driver-cloudstack

go 1.13

require (
	github.com/apache/cloudstack-go/v2 v2.15.0
	github.com/docker/docker v1.5.0 // indirect
	github.com/docker/machine v0.7.0
	github.com/xanzy/go-cloudstack/v2 v2.8.0
)

include:
  - template: Terraform/Base.gitlab-ci.yml
  - template: Jobs/SAST-IaC.gitlab-ci.yml

variables:
 TF_ROOT: terraform

stages:
  - validate
  - test
  - build
  - deploy
  - provision
  - run
  - cleanup

build-driver:
  image: golang:1.19.2-bullseye
  stage: build
  needs: []
  before_script:
    - go install github.com/mitchellh/gox@latest
  script:
    - cd docker-machine-driver-cloudstack
    - make
  artifacts:
    paths:
      - docker-machine-driver-cloudstack/dist/*
  tags:
    - ci.inria.fr
    - small

fmt:
  extends: .terraform:fmt
  needs: []
  tags:
    - ci.inria.fr
    - small

validate:
  extends: .terraform:validate
  needs: []
  tags:
    - ci.inria.fr
    - small

kics-iac-sast:
  extends: iac-sast
  image:
    name: "$SAST_ANALYZER_IMAGE"
  variables:
    SAST_ANALYZER_IMAGE_TAG: 3
    SAST_ANALYZER_IMAGE: "$SECURE_ANALYZERS_PREFIX/kics:$SAST_ANALYZER_IMAGE_TAG$SAST_IMAGE_SUFFIX"
  rules:
    - if: $SAST_DISABLED
      when: never
    - if: $SAST_EXCLUDED_ANALYZERS =~ /kics/
      when: never
    - if: $CI_COMMIT_BRANCH
  tags:
    - ci.inria.fr
    - small

.build:
  environment:
    name: $TF_STATE_NAME
    action: prepare
  extends: .terraform:build
  needs: []
  tags:
    - ci.inria.fr
    - small

build-test:
  variables:
    TF_STATE_NAME: test
    TF_CACHE_KEY: test
    TF_VAR_DRIVER_MACHINE_NAME: test-docker-machine-driver
  extends: .build

build-production-coq:
  variables:
    TF_STATE_NAME: production-coq
    TF_CACHE_KEY: production-coq
    TF_VAR_DRIVER_MACHINE_NAME: coq-docker-machine-driver
  extends: .build

build-production-mathcomp:
  variables:
    TF_STATE_NAME: production-mathcomp
    TF_CACHE_KEY: production-mathcomp
    TF_VAR_DRIVER_MACHINE_NAME: mathcomp-docker-machine-driver
  extends: .build

.deploy:
  stage: deploy
  environment:
    name: $TF_STATE_NAME
  script:
    - cd "${TF_ROOT}"
    - gitlab-terraform apply
  resource_group: ${TF_STATE_NAME}
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  tags:
    - ci.inria.fr
    - small

deploy-test:
  extends: .deploy
  dependencies:
    - build-test
  needs:
    - build-test
  variables:
    TF_STATE_NAME: test
    TF_CACHE_KEY: test

deploy-production-coq:
  extends: .deploy
  dependencies:
    - build-production-coq
  needs:
    - build-production-coq
  variables:
    TF_STATE_NAME: production-coq
    TF_CACHE_KEY: production-coq
  when: manual

deploy-production-mathcomp:
  extends: .deploy
  dependencies:
    - build-production-mathcomp
  needs:
    - build-production-mathcomp
  variables:
    TF_STATE_NAME: production-mathcomp
    TF_CACHE_KEY: production-mathcomp
  when: manual

.provision:
  stage: provision
  environment:
    name: $TF_STATE_NAME
    action: prepare
  before_script:
    - echo "Setup ssh-agent"
    - eval $(ssh-agent -s)
    - mkdir ~/.ssh
    - chmod 700 ~/.ssh
    - echo "${SSH_PRIVKEY}" | base64 -d > ~/.ssh/id_rsa
    - chmod 400 ~/.ssh/id_rsa
    - ssh-add ~/.ssh/id_rsa
    - apk add ansible python3
    - ansible-galaxy install riemers.gitlab-runner
    - ssh -vvv -i ~/.ssh/id_rsa -o StrictHostKeyChecking=no mden002@ci-ssh.inria.fr
    - ssh -vvv -i ~/.ssh/id_rsa -o StrictHostKeyChecking=no -o ProxyCommand="ssh -W $DRIVER_MACHINE_NAME:%p -q mden002@ci-ssh.inria.fr" ci@$DRIVER_MACHINE_NAME
  script:
    - cd "${CI_PROJECT_DIR}/ansible"
    - ansible-playbook -i $INVENTORY --user ci playbook.yml
  resource_group: ${TF_STATE_NAME}
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  tags:
    - ci.inria.fr
    - small
  when: manual

provision-test:
  extends: .provision
  needs:
    - build-driver
    - deploy-test
  dependencies:
    - build-driver
  variables:
    TF_STATE_NAME: test
    TF_CACHE_KEY: test
    INVENTORY: test-inventory
    DRIVER_MACHINE_NAME: test-docker-machine-driver

provision-production-coq:
  extends: .provision
  needs:
    - build-driver
    - deploy-production-coq
  dependencies:
    - build-driver
  variables:
    TF_STATE_NAME: production-coq
    TF_CACHE_KEY: production-coq
    INVENTORY: coq-inventory
    DRIVER_MACHINE_NAME: coq-docker-machine-driver
  when: manual

provision-production-mathcomp:
  extends: .provision
  needs:
    - build-driver
    - deploy-production-mathcomp
  dependencies:
    - build-driver
  variables:
    TF_STATE_NAME: production-mathcomp
    TF_CACHE_KEY: production-mathcomp
    INVENTORY: mathcomp-inventory
    DRIVER_MACHINE_NAME: mathcomp-docker-machine-driver
  when: manual

test-runner:
  stage: run
  script:
    - echo "Hello!"
  tags:
    - inria-cloudstack

destroy-test:
  extends: .terraform:destroy
  variables:
    TF_STATE_NAME: test
    TF_CACHE_KEY: test
  tags:
    - ci.inria.fr
    - small

destroy-production-coq:
  extends: .terraform:destroy
  variables:
    TF_STATE_NAME: production-coq
    TF_CACHE_KEY: production-coq
  tags:
    - ci.inria.fr
    - small
  when: manual
